Things to do before :
  - Enable Hyper-V feature through Control Panel -> Programs -> Programs and Features -> Turn Windows Feature on or off -> turn on “Hyper-V”
  - Download and install the latest version of Visual Studio.
  - Download and install Windows 10 SDK. (https://developer.microsoft.com/en-US/windows/downloads/windows-10-sdk)
  - Download or update to the latest version of Unity
  - Download and install the HoloLens 2 Emulator


1) Open Unity HUB and download 2019.4.31f1 version of Unity
2) Click on "ADD project", choose the file "lib" as the project
3) Open the project with the version you just download
4) When Unity is open, navigate on the project file : ASSETS/REALSENSESDK2.0/SCENES/SAMPLES/ and choose the scene you want
5) Now, on the menu bar, choose assets -> Import Package -> Customs Packages...
6) Double click on "Intel-RealSense" and click on "Import"

7) Now, on the menu bar, choose file -> Build Settings.
  Choose "Universal Windows Platform" : 

    Target Device : Hololens
    Architecture : ARM64
    Build TYPE : D3D Project
    Build and run on : USB device
    Build Config : Release


8) Now go on menu bar -> Assets -> Import Package -> Customs Packages... and choose Microsoft.MixedReality....Package
9) Import it

10) Follow instructions

11) Then, go on menu bar -> File -> Build Settings and "build"

12) Go where you build the app and double click on the solution file
13) When Visual Studio is open, choose Debug:x64 or x86:HololensEmulator2:version and start it
